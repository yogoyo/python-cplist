
PYTHON=python
STRIP=/usr/bin/strip -x

PYEXT_SUFFIX=$(shell $(PYTHON) -c "import sysconfig; print(sysconfig.get_config_var('EXT_SUFFIX'))")

cplist_ext=cplist$(PYEXT_SUFFIX)

targets = $(cplist_ext)

all: cplist

cplist: $(cplist_ext)

$(cplist_ext): plist.c setup.py
	$(PYTHON) setup.py build_ext -i
	$(STRIP) $@

.PHONY: clean
clean: 
	rm -rf $(targets) build
